import React from 'react';
import { Provider } from 'react-redux';
import { store } from './store';
import Dashboard from './containers/Dashboard';

import 'semantic-ui-css/semantic.min.css';

function App() {
  return (
    <Provider store={store}>
      <main>
        <Dashboard />
      </main>
    </Provider>
  );
}

export default App;
