// https://medium.com/@sanderdebr/converting-csv-to-a-2d-array-of-objects-94d43c56b12d
export function convertAdvertisingDataFromCSV(str) {
  const titles = str.slice(0, str.indexOf('\n')).split(',');
  const rows = str.slice(str.indexOf('\n') + 1).split('\n');
  return rows.map((row) => {
    const values = row.split(',');
    return titles.reduce((object, curr, i) => {
      if (curr === 'Clicks' || curr === 'Impressions') {
        // eslint-disable-next-line
        return (object[curr] = Number(values[i])), object;
      } else {
        // eslint-disable-next-line
        return (object[curr] = values[i]), object;
      }
    }, {});
  });
}
