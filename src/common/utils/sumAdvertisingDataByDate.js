import sumBy from 'lodash.sumby';
import groupBy from 'lodash.groupby';
import map from 'lodash.map';

export function sumAdvertisingDataByDate(data) {
  const groupByDate = groupBy(data, 'Date');
  return map(groupByDate, (objs, key) => {
    return {
      Date: key,
      Clicks: sumBy(objs, 'Clicks'),
      Impressions: sumBy(objs, 'Impressions'),
    };
  });
}
