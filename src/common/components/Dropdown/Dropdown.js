import React from 'react';
import { Dropdown as SemanticDropdown } from 'semantic-ui-react';
import PropTypes from 'prop-types';

export const Dropdown = ({ options, handleChange, multiple, placeholder }) => {
  const onChange = (event, { value = [] }) => {
    event.preventDefault();
    handleChange(value);
  };

  return (
    <SemanticDropdown
      onChange={onChange}
      fluid
      selection
      multiple={multiple}
      placeholder={placeholder}
      options={options.map((option) => ({
        key: option,
        text: option,
        value: option,
      }))}
    />
  );
};

Dropdown.propTypes = {
  options: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  placeholder: PropTypes.string,
};
