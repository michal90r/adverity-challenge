import Client from './Client.js';

export const GET_ADVERTISING_DATA_REQUEST = 'GET_ADVERTISING_DATA_REQUEST';
export const GET_ADVERTISING_DATA_SUCCESS = 'GET_ADVERTISING_DATA_SUCCESS';
export const GET_ADVERTISING_DATA_ERROR = 'GET_ADVERTISING_DATA_ERROR';
export const SET_FILTERS = 'SET_FILTERS';

const advertisingDataUrl =
  'http://adverity-challenge.s3-website-eu-west-1.amazonaws.com/DAMKBAoDBwoDBAkOBAYFCw.csv';

const getAdvertisingDataRequest = (payload) => ({
  type: GET_ADVERTISING_DATA_REQUEST,
  payload,
});

const getAdvertisingDataSuccess = (payload) => ({
  type: GET_ADVERTISING_DATA_SUCCESS,
  payload,
});

const getAdvertisingDataError = () => ({
  type: GET_ADVERTISING_DATA_ERROR,
});

export const setFilters = (payload) => ({
  type: SET_FILTERS,
  payload,
});

export const getAdvertisingData = () => {
  return function (dispatch) {
    dispatch(getAdvertisingDataRequest());
    Client.get(advertisingDataUrl)
      .then((response) => {
        dispatch(getAdvertisingDataSuccess(response));
      })
      .catch(() => {
        dispatch(getAdvertisingDataError());
      });
  };
};
