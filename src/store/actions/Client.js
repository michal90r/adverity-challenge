import axios from 'axios/index.js';

const Client = {
  get(url) {
    return this.useFetch('get', url);
  },

  post(url, data) {
    return this.useFetch('post', url, data);
  },

  put(url, data) {
    return this.useFetch('put', url, data);
  },

  delete(url, data) {
    return this.useFetch('delete', url, data);
  },

  useFetch(method, url, data) {
    return axios({
      method,
      url,
      data: JSON.stringify(data),
      headers: { 'content-type': 'application/json' },
    });
  },
};

export default Client;
