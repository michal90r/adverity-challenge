import uniq from 'lodash.uniq';
import map from 'lodash.map';
import {
  convertAdvertisingDataFromCSV,
  sumAdvertisingDataByDate,
} from 'common/utils';
import {
  GET_ADVERTISING_DATA_REQUEST,
  GET_ADVERTISING_DATA_ERROR,
  GET_ADVERTISING_DATA_SUCCESS,
  SET_FILTERS,
} from '../actions';

const initialState = {
  loading: false,
  error: false,
  rowData: [],
  aggregatedData: [],
  campaigns: [],
  datasources: [],
  filters: {
    selectedCampaigns: [],
    selectedDatasources: [],
  },
};

export function advertisingDataReducer(
  state = initialState,
  { type, error, payload }
) {
  switch (type) {
    case GET_ADVERTISING_DATA_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case GET_ADVERTISING_DATA_SUCCESS: {
      const { data } = payload;

      const rowData = convertAdvertisingDataFromCSV(data);
      const campaigns = uniq(map(rowData, 'Campaign'));
      const datasources = uniq(map(rowData, 'Datasource'));
      const aggregatedData = sumAdvertisingDataByDate(rowData);

      return {
        ...state,
        loading: false,
        rowData,
        aggregatedData,
        campaigns,
        datasources,
      };
    }
    case SET_FILTERS: {
      const { selectedCampaigns, selectedDatasources } = payload;
      const { rowData } = state;

      const filteredData = rowData.filter((row) => {
        const metCampaignCondition = selectedCampaigns.length
          ? selectedCampaigns.includes(row['Campaign'])
          : true;
        const metDatasourceCondition = selectedDatasources.length
          ? selectedDatasources.includes(row['Datasource'])
          : true;

        return metCampaignCondition && metDatasourceCondition;
      });

      const aggregatedData = sumAdvertisingDataByDate(filteredData);

      return {
        ...state,
        aggregatedData,
        filters: payload,
      };
    }
    case GET_ADVERTISING_DATA_ERROR: {
      return {
        ...state,
        loading: false,
      };
    }
    default: {
      return state;
    }
  }
}
