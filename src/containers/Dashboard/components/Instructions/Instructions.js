import React from 'react';

import './Instructions.scss';

export const Instructions = () => (
  <div className="instructions">
    <ul className="instructions__list">
      <li>Select zero to N Datasources</li>
      <li>Select zero to N Campaigns</li>
    </ul>

    <p className="instructions__small-text">(where 0 means "All")</p>

    <p>
      Hitting "Apply", filters the chart to show a timeseries for both{' '}
      <em>Clicks</em> and <em>Impressions</em> for given <em>Datasources</em>{' '}
      and <em>Campaigns</em> - logical AND
    </p>
  </div>
);
