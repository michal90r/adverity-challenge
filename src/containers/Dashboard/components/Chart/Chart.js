import PropTypes from 'prop-types';
import React from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';

import './Chart.scss';

export class Chart extends React.PureComponent {
  render() {
    const {
      aggregatedData,
      filters: { selectedCampaigns, selectedDatasources },
    } = this.props;

    return (
      <div className="chart">
        <div className="chart__title">
          <span>{`${
            selectedDatasources.length
              ? `Datasource "${selectedDatasources.join('" and "')}"`
              : 'All Datasources'
          }; `}</span>
          <span>{`${
            selectedCampaigns.length
              ? `Campaign "${selectedCampaigns.join('" and "')}"`
              : 'All Campaigns'
          }`}</span>
        </div>

        <div className="chart__line-chart">
          <LineChart
            width={800}
            height={600}
            data={aggregatedData}
            margin={{
              top: 20,
              right: 20,
              left: 20,
              bottom: 0,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />

            <Tooltip />

            <Legend />

            <YAxis
              yAxisId="left"
              domain={[0, 'auto']}
              stroke="#8884d8"
              dataKey="Clicks"
            />
            <Line
              yAxisId="left"
              label="Clicks"
              type="monotone"
              stroke="#8884d8"
              dataKey="Clicks"
              dot={false}
            />

            <YAxis
              yAxisId="right"
              orientation="right"
              domain={[0, 'auto']}
              stroke="#82ca9d"
              dataKey="Impressions"
            />
            <Line
              yAxisId="right"
              label="Impressions"
              type="monotone"
              stroke="#82ca9d"
              dataKey="Impressions"
              dot={false}
            />

            <XAxis dataKey="Date" minTickGap={60} />
          </LineChart>
        </div>
      </div>
    );
  }
}

export const FiltersPropTypes = PropTypes.exact({
  selectedCampaigns: PropTypes.array.isRequired,
  selectedDatasources: PropTypes.array.isRequired,
});

Chart.propTypes = {
  aggregatedData: PropTypes.array.isRequired,
  filters: FiltersPropTypes,
};
