import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Button } from 'common/components';

import './Filters.scss';

export const Filters = ({ datasources, campaigns, onSubmit }) => {
  const [selectedDatasources, setSelectedDatasources] = useState([]);
  const [selectedCampaigns, setSelectedCampaigns] = useState([]);

  const apply = () => {
    onSubmit({
      selectedDatasources,
      selectedCampaigns,
    });
  };

  return (
    <div className="filters">
      <h3>Filter dimension values</h3>
      <div className="filters__flex-container">
        <div className="filters__panel">
          <div className="filters__method">
            <h4 className="filters__caption">Datasource</h4>

            <Dropdown
              options={datasources}
              handleChange={setSelectedDatasources}
              placeholder={'All'}
              multiple
            />
          </div>

          <div className="filters__method">
            <h4 className="filters__caption">Campaign</h4>

            <Dropdown
              options={campaigns}
              handleChange={setSelectedCampaigns}
              placeholder={'All'}
              multiple
            />
          </div>
        </div>

        <div className="filters__apply-button">
          <Button onClick={apply}>Apply</Button>
        </div>
      </div>
    </div>
  );
};

Filters.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  datasources: PropTypes.array.isRequired,
  campaigns: PropTypes.array.isRequired,
};
