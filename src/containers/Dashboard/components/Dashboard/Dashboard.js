import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Instructions } from '../Instructions';
import { Filters } from '../Filters';
import { Chart, FiltersPropTypes } from '../Chart';

import './Dashboard.scss';

export const Dashboard = ({
  campaigns,
  datasources,
  aggregatedData,
  setFilters,
  getAdvertisingData,
  filters,
}) => {
  useEffect(() => {
    getAdvertisingData();
  }, [getAdvertisingData]);

  return (
    <div className="dashboard">
      <Instructions />
      <div className="dashboard__chart-panel">
        <Filters
          datasources={datasources}
          campaigns={campaigns}
          onSubmit={setFilters}
        />

        <Chart aggregatedData={aggregatedData} filters={filters} />
      </div>
    </div>
  );
};

Dashboard.propTypes = {
  filters: FiltersPropTypes,
  loading: PropTypes.bool.isRequired,
  getAdvertisingData: PropTypes.func.isRequired,
  aggregatedData: PropTypes.array.isRequired,
  setFilters: PropTypes.func.isRequired,
  campaigns: PropTypes.array.isRequired,
  datasources: PropTypes.array.isRequired,
};
