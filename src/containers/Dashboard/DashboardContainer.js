import { connect } from 'react-redux';
import { getAdvertisingData, setFilters } from 'store/actions';
import { Dashboard } from './components/Dashboard';

const mapStateToProps = ({ advertisingDataReducer }) => ({
  aggregatedData: advertisingDataReducer.aggregatedData,
  campaigns: advertisingDataReducer.campaigns,
  datasources: advertisingDataReducer.datasources,
  filters: advertisingDataReducer.filters,
  loading: advertisingDataReducer.loading,
});

const mapDispatchToProps = (dispatch) => ({
  getAdvertisingData: () => dispatch(getAdvertisingData()),
  setFilters: (filters) => dispatch(setFilters(filters)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
