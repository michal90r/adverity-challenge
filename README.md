This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Chosen solution

A) React Frontend only


### Start project

Run: <br />
* `yarn install`

* `yarn start`

* open chrome in disabled web security mode ( due to CORS policy on S3 bucket ), onMac:<br />
`open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security`

* Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### Possible improvements for a solution as a challenge 

* Implement loading indicators, `isLoading` flags are ready in the reducer
* Implement `clear filters` and `revert selection` buttons for filters

### Possible improvements for a solution as a production app 

* TypeScript or Flow, PropTypes was quicker solution but not strict enough for complex apps
* Move heavy calculations to BE
* More generic utils, hard to reuse
* Custom DropDowns or better library, Semantic-UI Dropdowns throw errors to the console
* Responsive chart
* Error handling
* Windowing for campaign dropdown [react-window](https://react-window.now.sh/#/examples/list/fixed-size) [react-virtualized](https://bvaughn.github.io/react-virtualized/#/components/List)
* replace redux-thunk with redux-saga or RxJS if more complex actions are needed
* replace Recharts with D3 if more customizable charts are needed
* it is possible to optimise calculations for advertising data ( fewer iterations over big dataset )